#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W\$ '

export PATH="$(yarn global bin):$PATH"

if command -v tmux >/dev/null 2>&1 && [ "${DISPLAY}" ]; then
    # If not inside a tmux session, and if no session is started, start a session
    [ -z "${TMUX}" ] && tmux
fi

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    exec startx
fi
