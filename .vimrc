syntax on

set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set autoindent
set backspace=indent,eol,start

set number

set ignorecase
set smartcase

set laststatus=2

set visualbell

set ruler

let g:powerline_pycmd="py3"
