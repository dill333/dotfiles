# dotfiles

My Arch Linux dotfiles and install script.

Run at your own risk

How to run this:

- Follow the Arch Linux Installation Guide until you chroot into the new system (<https://wiki.archlinux.org/index.php/Installation_guide>)

- Install wget (either during the pacstrap step, or after you chroot into the new system by running pacman -S wget)

- wget the install script (<https://gitlab.com/dill333/dotfiles/-/raw/master/install.sh>)

- chmod +x the install script

- Run the install script

Wallpaper cut from: <https://www.artstation.com/artwork/KJvWG>
