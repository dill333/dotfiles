#/bin/bash

# TODO: Stop duplicating valid options

default_user=dylan

set_timezone() {
    echo "Setting timezone to America/Chicago"
    ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
    hwclock --systohc
}

set_localization() {
    echo "Setting localization"
    sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
    locale-gen
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf
}

set_hostname() {
    echo "Enter hostname:"
    read hostname
    echo "$hostname" >> /etc/hostname
    echo -e "127.0.0.1\tlocalhost\n::1\tlocalhost\n127.0.1.1\t$hostname.localdomain\t$hostname" >> /etc/hosts
}

update_packages_pacman() {
    pacman -Syu --noconfirm
}

configure_network() {
    pacman -S networkmanager --noconfirm
    systemctl enable NetworkManager
    systemctl start NetworkManager
}

create_initramfs() {
    mkinitcpio -P
}

set_password() {
    echo "Set root password"
    passwd root
}

setup_bootloader() {
    echo "Setting up bootloader (GRUB)"
    pacman -S grub efibootmgr ntfs-3g os-prober --noconfirm
    grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
    grub-mkconfig -o /boot/grub/grub.cfg
}

setup_user() {
    echo "Installing and configuring sudo"
    pacman -S sudo --noconfirm
    sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

    echo "Creating default user ($default_user)"
    useradd -m -G wheel $default_user
    passwd $default_user

    echo "Disabling root login"
    passwd -l root
}

install_yay() {
    sudo -u $default_user -H sh -c "sudo pacman -S git binutils make gcc pkg-config fakeroot --noconfirm; \
    mkdir ~/aur; \
    git clone https://aur.archlinux.org/yay.git ~/aur/yay; \
    cd ~/aur/yay; \
    makepkg -si --noconfirm; \
    cd ~; \
    rm -rf ~/aur"
}

install_default() {
    sudo -u $default_user -H sh -c "yay --noconfirm; \
    yay -S base-devel vim tmux man-db xorg xorg-xinit xorg-drivers bspwm sxhkd termite \
    rofi powerline powerline-fonts-git powerline-vim polybar siji papirus-icon-theme \
    picom feh ranger ueberzug openssh firefox-developer-edition lxrandr-gtk3 \
    pulseaudio-alsa ncmpcpp mopidy mopidy-mpd mopidy-spotify xbindkeys betterlockscreen \
    htop powertop visual-studio-code-bin dotnet-host-bin dotnet-sdk-bin aspnet-runtime-bin \
    docker docker-compose npm yarn redshift otf-ipafont nerd-fonts-fira-code vi --noconfirm; \
    yarn global add @vue/cli;"
    
    echo "Enabling services"
    sudo -u root -H sh -c "systemctl enable docker;"
}

install_dotfiles() {
    sudo -u $default_user -H sh -c "cd ~; \
    if [ -d ~/dotfiles ]; then \
        cd ~/dotfiles; \
        git pull; \
        git submodule update --init --recursive; \
    else \
        git clone --recurse-submodules https://gitlab.com/dill333/dotfiles.git; \
        cd ~/dotfiles; \
    fi; \
    rm ~/.vimrc; \
    ln -s ~/dotfiles/.vimrc ~/.vimrc; \
    rm ~/.bashrc; \
    ln -s ~/dotfiles/.bashrc ~/.bashrc; \
    rm ~/.xinitrc; \
    ln -s ~/dotfiles/.xinitrc ~/.xinitrc; \
    rm ~/.tmux.conf; \
    ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf; \
    rm -r ~/.tmux; \
    ln -s ~/dotfiles/.tmux ~/.tmux; \
    rm -r ~/.config; \
    ln -s ~/dotfiles/.config ~/.config; \
    rm ~/.Xresources; \
    ln -s ~/dotfiles/.Xresources ~/.Xresources; \
    rm ~/.wallpaper.png; \
    ln -s ~/dotfiles/.wallpaper.png ~/.wallpaper.png; \
    betterlockscreen -u ~/.wallpaper.png; \
    rm ~/.xbindkeysrc; \
    ln -s ~/dotfiles/.xbindkeysrc ~/.xbindkeysrc;"
}

echo "Assumptions:"
echo "- Boot mode is EFI"
echo "- System clock is accurate"
echo "- Disks already partitioned"
echo "- Partitions already formatted"
echo "- Root partition mounted"
echo "- base, linux, and linux-firmware packages already installed (via pacstrap)"
echo "- /etc/fstab generated (via genfstab)"
echo "- chroot'ed into new system"

echo "Options (can input multiple with commas, e.g. 1, 2, 3)"
echo "1. Set Timezone"
echo "2. Set Localization"
echo "3. Set hostname and configure hosts file"
echo "4. Update packages (via pacman)"
echo "5. Configure network"
echo "6. Create initramfs"
echo "7. Set password"
echo "8. Setup boot loader (GRUB)"
echo "9. Default user setup ($default_user), install/configure sudo, disable root login (required for next steps)"
echo "10. Install yay (required for package installs)"
echo "11. Install default packages"
echo "12. Download and configure dotfiles for default user ($default_user)"
echo "99. All of the above"
echo "-1. Exit"

read option

# Remove all spaces
option=`echo "$option" | sed 's/[[:blank:]]//g'`

# Split by comma and get each option individually
IFS=","
read -ra options_array <<< "$option"

# Validate all options are valid, or if any of them are -1 (exit)
all="0"
for i in "${options_array[@]}"
do
    case "$i" in
    "-1")
        echo "Exiting"
        exit 0
        ;;
    "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" | "10" | "11" | "12")
        ;;
    "99")
        all="1"
        ;;
    *)
        echo "Unknown option $i"
        exit 1
        ;;
    esac
done

# Run each option
if [[ "$all" == "1" ]]; then
    options_array=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12")
fi

for i in "${options_array[@]}"
do
    case "$i" in
    "1")
        set_timezone
        ;;
    "2")
        set_localization
        ;;
    "3")
        set_hostname
        ;;
    "4")
        update_packages_pacman
        ;;
    "5")
        configure_network
        ;;
    "6")
        create_initramfs
        ;;
    "7")
        set_password
        ;;
    "8")
        setup_bootloader
        ;;
    "9")
        setup_user
        ;;
    "10")
        install_yay
        ;;
    "11")
        install_default
        ;;
    "12")
        install_dotfiles
        ;;
    esac
done

exit 0
